class RemoveObjectIdAndObjectTypeFromActivity < ActiveRecord::Migration
  def change
    remove_columns :activities, :object_id, :object_type
  end
end
