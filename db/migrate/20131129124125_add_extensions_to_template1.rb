class AddExtensionsToTemplate1 < ActiveRecord::Migration


  def change
   system(%q/psql -d template1 -U postgres -c "create extension  if not exists  btree_gin;create extension  if not exists  btree_gist; create extension  if not exists \"uuid-ossp\"; create extension  if not exists  citext; create extension  if not exists  hstore;"/) if Rails.env.development?

  end
end
