class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.string :name
      t.string :description
      t.string :flickr_album_url
      t.string :youtube_urls , array: true
      t.belongs_to :event

      t.timestamps
    end
  end
end
