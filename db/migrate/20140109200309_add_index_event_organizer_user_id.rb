class AddIndexEventOrganizerUserId < ActiveRecord::Migration
  def change
	add_index :event_organizers , :user_id
  end
end
