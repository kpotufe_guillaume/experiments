class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :description
      t.boolean :public?
      t.datetime :startdate
      t.datetime :enddate
      t.string :current_status
      t.belongs_to :user
      t.belongs_to :group
      t.belongs_to :event_category

      t.timestamps
    end
  end
end
