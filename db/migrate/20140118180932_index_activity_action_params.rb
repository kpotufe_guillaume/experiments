class IndexActivityActionParams < ActiveRecord::Migration
  def change
    add_index :activities, :action_params, using: :gin
  end
end
