class AddIndexGivenEntrysUserId < ActiveRecord::Migration
  def change
	add_index :given_entries , :user_id
  end
end
