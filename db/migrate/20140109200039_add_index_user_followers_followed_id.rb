class AddIndexUserFollowersFollowedId < ActiveRecord::Migration
  def change
	add_index :user_followers , :followed_id
  end
end
