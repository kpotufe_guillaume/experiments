class RemoveCoordinatesFromLocation < ActiveRecord::Migration
  def change
    remove_column :locations, :coordinates, :point
  end
end
