class CreateDeliveryModes < ActiveRecord::Migration
  def change
    create_table :delivery_modes do |t|
      t.string :name
      t.string :description
      t.belongs_to :provider

      t.timestamps
    end
  end
end
