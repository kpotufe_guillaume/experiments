class CreateGivenEntries < ActiveRecord::Migration
  def change
    create_table :given_entries do |t|
      t.belongs_to :user
      t.belongs_to :entry

      t.timestamps
    end
  end
end
