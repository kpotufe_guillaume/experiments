class CreateGiftCategories < ActiveRecord::Migration
  def change
    create_table :gift_categories do |t|
      t.string :name
      t.string :description
      t.integer :parent_id

      t.timestamps
    end
  end
end
