class AddIndexLocationsGeolocalizableType < ActiveRecord::Migration
  def change
	add_index :locations , :geolocalizable_type
  end
end
