class CreateThemes < ActiveRecord::Migration
  def change
    create_table :themes do |t|
      t.string :background_image_url
      t.text :css
      t.text :html_content
      t.belongs_to :event

      t.timestamps
    end
  end
end
