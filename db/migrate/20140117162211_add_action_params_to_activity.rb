class AddActionParamsToActivity < ActiveRecord::Migration
  def change
    add_column :activities, :action_params, :hstore, index: true
  end
end
