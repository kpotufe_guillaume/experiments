class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.string :description
      t.text :pricing_info_markup
      t.string :contact
      t.belongs_to :user
      t.belongs_to :event_category

      t.timestamps
    end
  end
end
