class AddIndexLocationsGeolocalizableId < ActiveRecord::Migration
  def change
	add_index :locations , :geolocalizable_id
  end
end
