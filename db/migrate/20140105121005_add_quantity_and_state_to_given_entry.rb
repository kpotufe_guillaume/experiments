class AddQuantityAndStateToGivenEntry < ActiveRecord::Migration
  def change
    add_column :given_entries, :quantity, :integer
    add_column :given_entries, :state, :string
  end
end
