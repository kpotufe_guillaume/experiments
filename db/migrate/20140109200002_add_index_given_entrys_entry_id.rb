class AddIndexGivenEntrysEntryId < ActiveRecord::Migration
  def change
	add_index :given_entries , :entry_id
  end
end
