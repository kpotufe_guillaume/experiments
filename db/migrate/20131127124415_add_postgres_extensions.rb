class AddPostgresExtensions < ActiveRecord::Migration
  def change
    execute '
      CREATE EXTENSION  IF NOT EXISTS  "btree_gin";
      CREATE EXTENSION  IF NOT EXISTS  "btree_gist";
      CREATE EXTENSION  IF NOT EXISTS  "uuid-ossp";
      CREATE EXTENSION  IF NOT EXISTS  "citext";
      CREATE EXTENSION  IF NOT EXISTS  "hstore";'

  end
end
