class AddBasicDetailsToUser < ActiveRecord::Migration
  def change
    add_column :users, :birthday, :string
    add_column :users, :phone_number, :string
  end
end
