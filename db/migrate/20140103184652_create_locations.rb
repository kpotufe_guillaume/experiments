class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.string :description
      t.string :picture_url
      t.column :coordinates, :point
      t.references :geolocalizable, polymorphic: true

      t.timestamps
    end
  end
end
