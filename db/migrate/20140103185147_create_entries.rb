class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.integer :quantity
      t.string :state
      t.boolean :discreet?
      t.datetime :deadline
      t.string :description
      t.belongs_to :event
      t.belongs_to :gift

      t.timestamps
    end
  end
end
