class CreateGifts < ActiveRecord::Migration
  def change
    create_table :gifts do |t|
      t.string :name
      t.string :description
      t.string :picture_url
      t.money :price
      t.belongs_to :event_category
      t.belongs_to :gift_category
      t.belongs_to :provider

      t.timestamps
    end
  end
end
