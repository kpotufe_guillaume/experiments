class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.belongs_to :user, index: true
      t.string :subject_class
      t.integer :subject_id
      t.string :actions, array:true, default:[]

      t.timestamps
    end
    add_index  :permissions, :actions, using: 'gin'
  end
end
