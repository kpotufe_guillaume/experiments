class AddIndexDeliveryModesProviderId < ActiveRecord::Migration
  def change
	add_index :delivery_modes , :provider_id
  end
end
