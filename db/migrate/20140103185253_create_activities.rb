class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :object_type
      t.string :object_id
      t.string :action
      t.references :active, polymorphic: true

      t.timestamps
    end
  end
end
