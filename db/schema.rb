# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140201010059) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "btree_gin"
  enable_extension "btree_gist"
  enable_extension "uuid-ossp"
  enable_extension "citext"
  enable_extension "hstore"

  create_table "activities", force: true do |t|
    t.string   "action"
    t.integer  "active_id"
    t.string   "active_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.hstore   "action_params"
  end

  add_index "activities", ["action_params"], name: "index_activities_on_action_params", using: :gin
  add_index "activities", ["active_id"], name: "index_activities_on_active_id", using: :btree
  add_index "activities", ["active_type"], name: "index_activities_on_active_type", using: :btree
  add_index "activities", ["user_id"], name: "index_activities_on_user_id", using: :btree

  create_table "albums", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "flickr_album_url"
    t.string   "youtube_urls",     array: true
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "albums", ["event_id"], name: "index_albums_on_event_id", using: :btree
  add_index "albums", ["youtube_urls"], name: "index_albums_on_youtube_urls", using: :btree

  create_table "circle_users", force: true do |t|
    t.integer  "user_id"
    t.integer  "circle_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "circle_users", ["circle_id"], name: "index_circle_users_on_circle_id", using: :btree
  add_index "circle_users", ["user_id"], name: "index_circle_users_on_user_id", using: :btree

  create_table "circles", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "circles", ["user_id"], name: "index_circles_on_user_id", using: :btree

  create_table "conversations", force: true do |t|
    t.string   "subject",    default: ""
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "delivery_modes", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "provider_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delivery_modes", ["provider_id"], name: "index_delivery_modes_on_provider_id", using: :btree

  create_table "entries", force: true do |t|
    t.integer  "quantity"
    t.string   "state"
    t.boolean  "discreet?"
    t.datetime "deadline"
    t.string   "description"
    t.integer  "event_id"
    t.integer  "gift_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "entries", ["event_id"], name: "index_entries_on_event_id", using: :btree
  add_index "entries", ["gift_id"], name: "index_entries_on_gift_id", using: :btree

  create_table "event_categories", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "parent_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "event_categories", ["parent_id"], name: "index_event_categories_on_parent_id", using: :btree

  create_table "event_followers", force: true do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "event_followers", ["event_id"], name: "index_event_followers_on_event_id", using: :btree
  add_index "event_followers", ["user_id"], name: "index_event_followers_on_user_id", using: :btree

  create_table "event_organizers", force: true do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "event_organizers", ["event_id"], name: "index_event_organizers_on_event_id", using: :btree
  add_index "event_organizers", ["user_id"], name: "index_event_organizers_on_user_id", using: :btree

  create_table "events", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.boolean  "public?"
    t.datetime "startdate"
    t.datetime "enddate"
    t.string   "current_status"
    t.integer  "user_id"
    t.integer  "group_id"
    t.integer  "event_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "events", ["event_category_id"], name: "index_events_on_event_category_id", using: :btree
  add_index "events", ["group_id"], name: "index_events_on_group_id", using: :btree
  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

  create_table "gift_categories", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "parent_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "gift_categories", ["parent_id"], name: "index_gift_categories_on_parent_id", using: :btree

  create_table "gifts", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "picture_url"
    t.integer  "price_cents",       default: 0,     null: false
    t.string   "price_currency",    default: "USD", null: false
    t.integer  "event_category_id"
    t.integer  "gift_category_id"
    t.integer  "provider_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "gifts", ["event_category_id"], name: "index_gifts_on_event_category_id", using: :btree
  add_index "gifts", ["gift_category_id"], name: "index_gifts_on_gift_category_id", using: :btree
  add_index "gifts", ["provider_id"], name: "index_gifts_on_provider_id", using: :btree

  create_table "given_entries", force: true do |t|
    t.integer  "user_id"
    t.integer  "entry_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quantity"
    t.string   "state"
  end

  add_index "given_entries", ["entry_id"], name: "index_given_entries_on_entry_id", using: :btree
  add_index "given_entries", ["user_id"], name: "index_given_entries_on_user_id", using: :btree

  create_table "group_users", force: true do |t|
    t.integer  "user_id"
    t.integer  "group_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "group_users", ["group_id"], name: "index_group_users_on_group_id", using: :btree
  add_index "group_users", ["user_id"], name: "index_group_users_on_user_id", using: :btree

  create_table "groups", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "groups", ["user_id"], name: "index_groups_on_user_id", using: :btree

  create_table "locations", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "picture_url"
    t.integer  "geolocalizable_id"
    t.string   "geolocalizable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "address"
  end

  add_index "locations", ["geolocalizable_id"], name: "index_locations_on_geolocalizable_id", using: :btree
  add_index "locations", ["geolocalizable_type"], name: "index_locations_on_geolocalizable_type", using: :btree
  add_index "locations", ["latitude"], name: "index_locations_on_latitude", using: :btree
  add_index "locations", ["longitude"], name: "index_locations_on_longitude", using: :btree

  create_table "notifications", force: true do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",              default: ""
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "conversation_id"
    t.boolean  "draft",                default: false
    t.datetime "updated_at",                           null: false
    t.datetime "created_at",                           null: false
    t.integer  "notified_object_id"
    t.string   "notified_object_type"
    t.string   "notification_code"
    t.string   "attachment"
    t.boolean  "global",               default: false
    t.datetime "expires"
  end

  add_index "notifications", ["conversation_id"], name: "index_notifications_on_conversation_id", using: :btree

  create_table "permissions", force: true do |t|
    t.integer  "user_id"
    t.string   "subject_class"
    t.integer  "subject_id"
    t.string   "actions",       default: [], array: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "permissions", ["actions"], name: "index_permissions_on_actions", using: :gin
  add_index "permissions", ["user_id"], name: "index_permissions_on_user_id", using: :btree

  create_table "providers", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "providers", ["user_id"], name: "index_providers_on_user_id", using: :btree

  create_table "rails_admin_histories", force: true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      limit: 2
    t.integer  "year",       limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], name: "index_rails_admin_histories", using: :btree

  create_table "rates", force: true do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "stars",         null: false
    t.string   "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rates", ["rateable_id", "rateable_type"], name: "index_rates_on_rateable_id_and_rateable_type", using: :btree
  add_index "rates", ["rater_id"], name: "index_rates_on_rater_id", using: :btree

  create_table "rating_caches", force: true do |t|
    t.integer  "cacheable_id"
    t.string   "cacheable_type"
    t.float    "avg",            null: false
    t.integer  "qty",            null: false
    t.string   "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rating_caches", ["cacheable_id", "cacheable_type"], name: "index_rating_caches_on_cacheable_id_and_cacheable_type", using: :btree

  create_table "receipts", force: true do |t|
    t.integer  "receiver_id"
    t.string   "receiver_type"
    t.integer  "notification_id",                            null: false
    t.boolean  "is_read",                    default: false
    t.boolean  "trashed",                    default: false
    t.boolean  "deleted",                    default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "receipts", ["notification_id"], name: "index_receipts_on_notification_id", using: :btree

  create_table "services", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.text     "pricing_info_markup"
    t.string   "contact"
    t.integer  "user_id"
    t.integer  "event_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "services", ["event_category_id"], name: "index_services_on_event_category_id", using: :btree
  add_index "services", ["user_id"], name: "index_services_on_user_id", using: :btree

  create_table "themes", force: true do |t|
    t.string   "background_image_url"
    t.text     "css"
    t.text     "html_content"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "themes", ["event_id"], name: "index_themes_on_event_id", using: :btree

  create_table "user_followers", force: true do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_followers", ["followed_id"], name: "index_user_followers_on_followed_id", using: :btree
  add_index "user_followers", ["follower_id"], name: "index_user_followers_on_follower_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.integer  "roles_mask"
    t.string   "birthday"
    t.string   "phone_number"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "notifications", "conversations", name: "notifications_on_conversation_id"

  add_foreign_key "receipts", "notifications", name: "receipts_on_notification_id"

end
