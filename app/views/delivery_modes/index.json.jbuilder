json.array!(@delivery_modes) do |delivery_mode|
  json.extract! delivery_mode, :id
  json.url delivery_mode_url(delivery_mode, format: :json)
end
