json.array!(@gift_categories) do |gift_category|
  json.extract! gift_category, :id
  json.url gift_category_url(gift_category, format: :json)
end
