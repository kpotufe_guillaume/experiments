json.array!(@users) do |user|
  json.extract! user, :id
  json.url zwajuser_url(user, format: :json)
end
