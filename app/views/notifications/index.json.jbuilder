json.array!(@notifications) do |notification|
  json.extract! notification, :id, :subject
  json.url notification_url(notification, format: :json)
end
