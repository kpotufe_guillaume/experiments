class GiftCategoriesController < ApplicationController
  before_action :set_gift_category, only: [:show, :edit, :update, :destroy]

  # GET /gift_categories
  # GET /gift_categories.json
  def index
    @gift_categories = GiftCategory.all
  end

  # GET /gift_categories/1
  # GET /gift_categories/1.json
  def show
  end

  # GET /gift_categories/new
  def new
    @gift_category = GiftCategory.new
  end

  # GET /gift_categories/1/edit
  def edit
  end

  # POST /gift_categories
  # POST /gift_categories.json
  def create
    @gift_category = GiftCategory.new(gift_category_params)

    respond_to do |format|
      if @gift_category.save
        format.html { redirect_to @gift_category, notice: 'Gift category was successfully created.' }
        format.json { render action: 'show', status: :created, location: @gift_category }
      else
        format.html { render action: 'new' }
        format.json { render json: @gift_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /gift_categories/1
  # PATCH/PUT /gift_categories/1.json
  def update
    respond_to do |format|
      if @gift_category.update(gift_category_params)
        format.html { redirect_to @gift_category, notice: 'Gift category was successfully updated.' }
        format.json { render action: 'show', status: :ok, location: @gift_category }
      else
        format.html { render action: 'edit' }
        format.json { render json: @gift_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gift_categories/1
  # DELETE /gift_categories/1.json
  def destroy
    @gift_category.destroy
    respond_to do |format|
      format.html { redirect_to gift_categories_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gift_category
      @gift_category = GiftCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gift_category_params
      params[:gift_category].permit(:name, :description, :parent_id)
    end
end
