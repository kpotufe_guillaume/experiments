class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #Secure all access

  before_filter :authenticate_user!
  before_filter do
    #:authenticate_user!
    resource = controller_name.singularize.to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end
  #load_and_authorize_resource if :user_signed_in?
  #authorize_resource #class: false


  rescue_from CanCan::AccessDenied do |exception|
    redirect_to main_app.root_path, :alert => exception.message
  end
  before_filter :configure_permitted_parameters, if: :devise_controller?





  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :name << :phone_number << :birthday 
  end


end
