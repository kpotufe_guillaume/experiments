class DeliveryModesController < ApplicationController
  before_action :set_delivery_mode, only: [:show, :edit, :update, :destroy]

  # GET /delivery_modes
  # GET /delivery_modes.json
  def index
    @delivery_modes = DeliveryMode.all
  end

  # GET /delivery_modes/1
  # GET /delivery_modes/1.json
  def show
  end

  # GET /delivery_modes/new
  def new
    @delivery_mode = DeliveryMode.new
  end

  # GET /delivery_modes/1/edit
  def edit
  end

  # POST /delivery_modes
  # POST /delivery_modes.json
  def create
    @delivery_mode = DeliveryMode.new(delivery_mode_params)

    respond_to do |format|
      if @delivery_mode.save
        format.html { redirect_to @delivery_mode, notice: 'Delivery mode was successfully created.' }
        format.json { render action: 'show', status: :created, location: @delivery_mode }
      else
        format.html { render action: 'new' }
        format.json { render json: @delivery_mode.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /delivery_modes/1
  # PATCH/PUT /delivery_modes/1.json
  def update
    respond_to do |format|
      if @delivery_mode.update(delivery_mode_params)
        format.html { redirect_to @delivery_mode, notice: 'Delivery mode was successfully updated.' }
        format.json { render action: 'show', status: :ok, location: @delivery_mode }
      else
        format.html { render action: 'edit' }
        format.json { render json: @delivery_mode.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /delivery_modes/1
  # DELETE /delivery_modes/1.json
  def destroy
    @delivery_mode.destroy
    respond_to do |format|
      format.html { redirect_to delivery_modes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_delivery_mode
      @delivery_mode = DeliveryMode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def delivery_mode_params
      params[:delivery_mode].permit(:name, :description, :provider_id)
    end
end
