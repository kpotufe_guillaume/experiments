class Location < ActiveRecord::Base
  validates :name, presence: true
  belongs_to :geolocalizable, polymorphic: true
  geocoded_by :address
  reverse_geocoded_by :latitude, :longitude
  after_validation :geocode,:geocode, :reverse_geocode #, if: ->(obj){ obj.address.present? and obj.address_changed? }
end
