class GiftCategory < ActiveRecord::Base

  validates :name, :description, presence: true

  belongs_to  :parent,        class_name: "GiftCategory"
  has_many    :subcategories, class_name: "GiftCategory", foreign_key: "parent_id"
  has_many    :gifts

end
