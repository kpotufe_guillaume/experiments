class Entry < ActiveRecord::Base

  belongs_to  :event
  belongs_to  :gift
  has_many    :given_entries

  def self.entry_states
    %w( open complete partial canceled promised )
  end


  validates :quantity,  numericality: { only_integer: true, greater_than: 0 }
  validates :state,     inclusion:    { in: entry_states}
  validates :discreet?, inclusion:    { in: [true, false] }

end
