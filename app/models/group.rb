class Group < ActiveRecord::Base

  validates :name, :description, :user_id , presence: true

  has_many    :locations,   as: :geolocalizable
  has_many    :activities,  as: :active
  has_many    :events
  belongs_to  :owner,       class_name: "User",   foreign_key: "user_id"
  has_many    :group_users
  has_many    :users,       through: :group_users

end
