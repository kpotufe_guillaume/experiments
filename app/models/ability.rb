require 'devise'
class Ability
  
  include CanCan::Ability

  def initialize(user)
    #alias_action  :put, to: :update
    specialcontrollers = [:session,:registration,:confirmation,:password,:welcome,:omniauth_callback,:zwajuser,:main,:rails_admin,:provider]
    specialclasses     = ['Provider']
    user ||= User.new # guest user (not logged in)
    if user.has_role? :admin
      can :manage, :all
    else
      #can :manage ,  specialcontrollers
      can :manage , [ Provider]
      #General create actions
      can :create ,[Event,Activity,Circle,Group,Entry,Gift,Provider,Service,Location,Album,EventCategory,
                   GiftCategory,Theme,GivenEntry,EventOrganizer,EventFollower,UserFollower,GroupUser,CircleUser ]

      #Create of relations : activate only if necessary.
      #can :create,[GivenEntry,EventOrganizer,EventFollower,UserFollower,GroupUser,CircleUser ]

      #Reads

      #
      #Specific actions
      can do |action, subject_class, subject, extra|
        #unless specialcontrollers.include? subject
        unless specialclasses.include? subject_class
          if subject_class == Symbol
            subject_class = subject.to_s.capitalize
            subject       = nil
          end
          #print "Setting Abilities" ,action, subject_class, subject, extra,'\n'
          #puts "subject is ",subject,"OOK",extra,"DONE"
          Permission.where("user_id =  ? and subject_class = ? and (subject_id is NULL or subject_id = ?) and ? && actions",
          user.id,subject_class,(subject.nil?  ? nil : subject.id),"{"+aliases_for_action(action).map(&:to_s).join(",")+"}" ).any?
        end
      end
    end

    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
    #can :read, :all                   # allow everyone to read everything	
  end
end
