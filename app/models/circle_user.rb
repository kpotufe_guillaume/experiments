class CircleUser < ActiveRecord::Base
  validates :user_id, :circle_id, presence: true
  belongs_to :user
  belongs_to :circle
end
