class DeliveryMode < ActiveRecord::Base

  validates :name, :description, :provider_id,  presence: true

  belongs_to  :provider
end
