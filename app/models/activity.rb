class Activity < ActiveRecord::Base



 # include Concerns::Zwajletsrate
  zwaj_letsrate_rateable "feelings"

  store_accessor :action_params, :object_id, :object_type

  belongs_to :active, polymorphic: true
  belongs_to :user



  def self.possible_actions
    %w( event_creation comment rating new_picture new_video  )
  end

  validates :action, :active_id, :active_type, :user_id  , presence: true
  validates :action, inclusion: {in: possible_actions}

end
