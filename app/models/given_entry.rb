class GivenEntry < ActiveRecord::Base
  validates :user_id, :entry_id, presence: true
  belongs_to :user
  belongs_to :entry
end
