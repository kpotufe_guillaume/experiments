class EventCategory < ActiveRecord::Base

  validates :name, :description, presence: true

  belongs_to  :parent,        class_name: "EventCategory"
  has_many    :subcategories, class_name: "EventCategory", foreign_key: "parent_id"
  has_many    :events
  has_many    :gifts
  has_many    :services

end
