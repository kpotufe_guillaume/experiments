require 'rubygems'
require 'role_model'

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  #acts_as_user :roles => :manager, :admin
  #CANARD REMOVED: acts_as_user :roles => [:manager, :admin] if ActiveRecord::Base.connected?

  has_many  :locations,           as: :geolocalizable,        dependent: :delete_all
  has_many  :activities,          as: :active                 #Activities that occured ON this user. ( user is the object. e.g he is rated)
  has_many  :authored_activities, class_name: "Activity"      #Activities that this user Authored. e.g this user Created a new event ...
  has_many  :providers
  has_many  :events
  has_many  :managed_groups,      class_name: "Group"
  has_many  :group_users
  has_many  :groups,              through: :group_users
  has_many  :circles,             dependent: :delete_all
  has_many  :given_entries
  has_many  :user_followers,      foreign_key: "followed_id"
  has_many  :followers,           through: :user_followers
  has_many  :followeds,           through: :user_followers

  acts_as_messageable
  letsrate_rater
  include RoleModel

  #Defined this to allows using it in application 
  def self.alias_protected_roles(*args)
    roles args
  end
  #roles  :manager, :admin     #Moved to application.rb because of Heroku database not yet initialized pbs.
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,:omniauthable, :omniauth_providers => [:facebook,:google_oauth2]
  #attr_accessible :provider, :uid, :name
  has_many :permissions  
  
  
  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    unless user
      user = User.create(name:auth.extra.raw_info.name,
                         provider:'facebook',
                         uid: (auth.uid.nil?)?(auth.info.email):(auth.uid),
                         email:auth.info.email,
                         password:Devise.friendly_token[0,20]
      )
    end
    user
  end

  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    data = access_token.info
    user = User.where(:email => data["email"]).first
    unless user
      user = User.create(name: data["name"],
                         email: data["email"],
                         uid: data["email"],
						             provider: 'google_oauth2',
                         password: Devise.friendly_token[0,20]
      )
    end
    user
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end
  
  
  def self.add_permission(u_id,subj_class,subj_id,cancan_action)
    permission = Permission.find_or_create_by!(user_id: u_id, subject_class: subj_class, subject_id:subj_id)
    unless (permission.actions.include? cancan_action)  
      permission.actions_will_change!
      permission.update_attributes!(:actions => permission.actions.push(cancan_action)) 
    end
  end

  def self.remove_permission(u_id,subj_class,subj_id,cancan_action)
    permission = Permission.find_by(user_id: u_id, subject_class: subj_class, subject_id:subj_id)
    if (permission and permission.actions.include?(cancan_action))
      permission.actions_will_change!
      permission.update_attributes!(:actions => permission.actions.delete(cancan_action))  
    end
  end

  def mailboxer_email(object)
    return self.email
  end

  def admin=(val)
    self.roles << :admin if(val and !self.admin?) 
  end
end

