class Gift < ActiveRecord::Base

  validates :name, :event_category_id, :gift_category_id, :provider_id, presence: true

  monetize :price_cents, allow_nil: true, numericality: { greater_than_or_equal_to: 0 }

  belongs_to  :gift_category
  belongs_to  :provider
  belongs_to  :event_category
end
