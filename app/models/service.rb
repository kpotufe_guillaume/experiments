class Service < ActiveRecord::Base

  validates :name, :description, :event_category_id, :pricing_info_markup, :contact, :user_id,  presence: true

  #include Concerns::Zwajletsrate
  zwaj_letsrate_rateable "feelings"

  belongs_to  :event_category
  belongs_to  :user
end
