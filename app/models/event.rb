class Event < ActiveRecord::Base

  #include Concerns::Zwajletsrate
  zwaj_letsrate_rateable "feelings"



  belongs_to  :event_category
  belongs_to  :group
  belongs_to  :user
  has_one     :theme,       dependent: :destroy
  has_one     :album,       dependent: :destroy
  has_many    :locations,   as: :geolocalizable,        dependent: :delete_all
  has_many    :activities,  as: :active
  has_many    :entries,     dependent: :destroy
  has_many    :event_followers,                         dependent: :destroy
  has_many    :event_organizers,                        dependent: :destroy
  has_many    :followers,   through: :event_followers,  class_name: "User", source: :user
  has_many    :organizers,  through: :event_organizers, class_name: "User", source: :user

  def self.event_states()
    %w(planned upcoming live finished canceled)
  end

  validates :name, :description,:event_category_id, :user_id, :startdate, :enddate, :current_status  , presence: true
  validates :public?,        inclusion: { in: [true, false] }
  validates :current_status, inclusion: { in: event_states  }
end
