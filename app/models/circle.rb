class Circle < ActiveRecord::Base

  validates :name, :description, :user_id , presence: true

  belongs_to  :owner,     class_name: "User",   foreign_key: "user_id"
  has_many    :circle_users
  has_many    :users,     through: :circle_users
end
