class AdminAbility
  include CanCan::Ability
  def initialize(user)
    if user and user.has_role?( :admin)
      can :access, :rails_admin       # only allow admin users to access Rails Admin
      can :dashboard                  # allow access to dashboard
      if user.has_role? :superadmin
        can :manage, :all             # allow superadmins to do anything
      elsif user.has_role? :manager
        can :manage, [User, Product]  # allow managers to do anything to products and users
      elsif user.has_role? :sales
        can :update, Product, :hidden => false  # allow sales to only update visible products
      end
    end
  end
end
