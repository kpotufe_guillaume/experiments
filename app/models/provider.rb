class Provider < ActiveRecord::Base

  validates :name, :description, :user_id, presence: true

 # include Concerns::Zwajletsrate
  zwaj_letsrate_rateable "feelings"

  belongs_to  :owner,           class_name: "User",   foreign_key: "user_id"
  has_many    :delivery_modes,  dependent: :destroy
  has_many    :gifts,           dependent: :destroy
end
