#require 'dalli'
#require "picasa"
require 'redis'
#For rating
ActiveRecord::Base.send(:include, Concerns::Zwajletsrate)

#Get Zwaj config
config_file = File.join(Rails.root, 'config', 'zwaj.yml')
ZWAJ_CONFIG = YAML.load(File.open(config_file))[Rails.env]

require "albums/zpictures"
#Authenticate with Picassa
PICASA_CLIENT    = Picasa::Client.new( user_id: ZWAJ_CONFIG['GOOGLE_USER'],password: ZWAJ_CONFIG['GOOGLE_PASSWORD'])
YOUTUBE_CLIENT   = YouTubeIt::Client.new(username: ZWAJ_CONFIG['GOOGLE_USER'], password: ZWAJ_CONFIG['GOOGLE_PASSWORD'], dev_key: ZWAJ_CONFIG['YOUTUBE_DEVELOPER_KEY'])
#Redis client
REDIS_PUB_CLIENT = Redis.new(url: ZWAJ_CONFIG['REDIS_URL'])  #, driver: :hiredis

=begin

  #ENV[key.to_s] = value
  #puts key,"=",value
# end if File.exists?(config_file)

##Initialize Memcachier server
#ENV["MEMCACHIER_SERVERS"] =ENV["ZWAJ_MEMCACHE_SERVERS"][0]["server"]
#ENV["MEMCACHIER_USERNAME"]=ENV["ZWAJ_MEMCACHE_SERVERS"][0]["user"] 
#ENV["MEMCACHIER_PASSWORD"]=ENV["ZWAJ_MEMCACHE_SERVERS"][0]["password"]
#
#cache = Dalli::Client.new(ENV["MEMCACHIER_SERVERS"].split(","),
#                    {:username => ENV["MEMCACHIER_USERNAME"],
#                     :password => ENV["MEMCACHIER_PASSWORD"],
#                     :failover => true,
#                     :socket_timeout => 1.5,
#                     :socket_failure_delay => 0.2
#                    })
require 'google/api_client'
require 'trollop'

YOUTUBE_API_SERVICE_NAME  = 'youtube'
YOUTUBE_API_VERSION     = 'v3'

scopes          = 'https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/youtube.upload'
client          = Google::APIClient.new
plus          = client.discovered_api('plus')
youtube         = client.discovered_api(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION)

key           = Google::APIClient::PKCS12.load_key('privatekey.p12', 'notasecret')
service_account     = Google::APIClient::JWTAsserter.new('305604533762-utt4s9nvcvaquenjbdtm1bqrr6odfgu2@developer.gserviceaccount.com',scopes,  key)

client.authorization  = service_account.authorize

#https://developers.google.com/youtube/v3/code_samples/ruby
=end
