require 'date'
FactoryGirl.define do
  
  trait :described do
    name        'name'
    description 'description'
  end
  
  factory :event_category_parent, class: EventCategory do
    described
  end

  factory :event_category do
    described
    association :parent, factory: :event_category_parent
  end

  factory :gift_category_parent, class: GiftCategory  do
    described
  end

  factory :gift_category  do
    described
    association :parent, factory: :gift_category_parent
  end

  factory :group do
    described
    association :owner, factory: :user
  end

  factory :activity do
    action    'comment'
    association :active, factory: :event
    user
  end

  factory :location do
    described
    association :geolocalizable, factory: :user
  end

  factory :permission do
  end


  factory :event do
    described
    event_category
    group
    startdate       { DateTime.now }
    enddate         { startdate + 5 }
    current_status  'planned'
    public?         true
    user
  end

  factory :album do
    described
    event
  end

  factory :circle do
    described
    association :owner, factory: :user
  end

  factory :service do
    described
    event_category
    contact   "Contact info"
    pricing_info_markup   "*Pricing info*"
    user
  end

  factory :gift do
    described
    event_category
    gift_category
    provider
    price_cents   500
  end

  factory :provider do
    described
    association :owner, factory: :user
  end

  factory :theme do
    event
  end

  factory :delivery_mode do
    described
    provider
  end

  factory :entry do
    event
    gift
    quantity  1
    state     'open'
    discreet? false
  end

end

