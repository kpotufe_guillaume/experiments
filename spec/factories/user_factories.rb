FactoryGirl.define do
  sequence :gen_email do |n|
    "user_tester#{n}@zwajjer.com"
  end

  # This will use the User class
  factory :new_user, class: User do
    name      'new_user_tester'
    email     {generate(:gen_email)}
    password  'zwaj$@2014'
    admin     false

    # This will guess the User class
    factory :user, aliases: [:owner]  do
      name      'user_tester'
      email     {generate(:gen_email)}
      # This will use the User class (Admin would have been guessed)
      factory :admin, class: User do
        name      'admin_tester'
        email     {generate(:gen_email)}
        admin     true
      end
    end
  end

end
