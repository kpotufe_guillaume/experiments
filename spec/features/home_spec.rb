require 'rspec'
require 'spec_helper'

describe 'Home page' do
  it 'allows signup logout and signin' do
    #Redirect to Sign in/up
    user = build_stubbed(:new_user)
    visit '/'
    expect(page).to  have_content('Sign Up Now')
    
    #Sign up
    within('#new_user') do
      fill_in 'user_name', with: user.name
      fill_in 'user_email', with: user.email
      fill_in 'user_password', with: user.password
      fill_in 'user_password_confirmation', with: user.password 
      click_on 'Sign up'
    end
    expect(page).to  have_content('Sign Out')
    
    #Log out
    within('.logout') do
      click_on 'sign_out_link'
    end
    expect(page).to  have_content('Sign in with:')
    
    #Sign in
    within('#login_form') do
      fill_in 'user_email', with: user.email
      fill_in 'user_password', with: user.password
      click_on 'Sign in'
    end
    expect(page).to  have_content('Sign Out')
  end
end
