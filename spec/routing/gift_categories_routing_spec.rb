require "spec_helper"

describe GiftCategoriesController do
  describe "routing" do

    it "routes to #index" do
      get("/gift_categories").should route_to("gift_categories#index")
    end

    it "routes to #new" do
      get("/gift_categories/new").should route_to("gift_categories#new")
    end

    it "routes to #show" do
      get("/gift_categories/1").should route_to("gift_categories#show", :id => "1")
    end

    it "routes to #edit" do
      get("/gift_categories/1/edit").should route_to("gift_categories#edit", :id => "1")
    end

    it "routes to #create" do
      post("/gift_categories").should route_to("gift_categories#create")
    end

    it "routes to #update" do
      put("/gift_categories/1").should route_to("gift_categories#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/gift_categories/1").should route_to("gift_categories#destroy", :id => "1")
    end

  end
end
