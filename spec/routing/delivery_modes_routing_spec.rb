require "spec_helper"

describe DeliveryModesController do
  describe "routing" do

    it "routes to #index" do
      get("/delivery_modes").should route_to("delivery_modes#index")
    end

    it "routes to #new" do
      get("/delivery_modes/new").should route_to("delivery_modes#new")
    end

    it "routes to #show" do
      get("/delivery_modes/1").should route_to("delivery_modes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/delivery_modes/1/edit").should route_to("delivery_modes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/delivery_modes").should route_to("delivery_modes#create")
    end

    it "routes to #update" do
      put("/delivery_modes/1").should route_to("delivery_modes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/delivery_modes/1").should route_to("delivery_modes#destroy", :id => "1")
    end

  end
end
