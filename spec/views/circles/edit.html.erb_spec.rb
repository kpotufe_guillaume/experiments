require 'spec_helper'

describe "circles/edit" do
  before(:each) do
    @circle = assign(:circle, stub_model(Circle))
  end

  it "renders the edit circle form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", circle_path(@circle), "post" do
    end
  end
end
