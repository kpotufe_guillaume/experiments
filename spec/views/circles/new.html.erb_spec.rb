require 'spec_helper'

describe "circles/new" do
  before(:each) do
    assign(:circle, stub_model(Circle).as_new_record)
  end

  it "renders new circle form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", circles_path, "post" do
    end
  end
end
