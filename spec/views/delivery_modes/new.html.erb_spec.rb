require 'spec_helper'

describe "delivery_modes/new" do
  before(:each) do
    assign(:delivery_mode, stub_model(DeliveryMode).as_new_record)
  end

  it "renders new delivery_mode form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", delivery_modes_path, "post" do
    end
  end
end
