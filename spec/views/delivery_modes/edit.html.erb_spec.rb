require 'spec_helper'

describe "delivery_modes/edit" do
  before(:each) do
    @delivery_mode = assign(:delivery_mode, stub_model(DeliveryMode))
  end

  it "renders the edit delivery_mode form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", delivery_mode_path(@delivery_mode), "post" do
    end
  end
end
