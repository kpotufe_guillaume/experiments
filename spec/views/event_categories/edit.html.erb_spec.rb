require 'spec_helper'

describe "event_categories/edit" do
  before(:each) do
    @event_category = assign(:event_category, stub_model(EventCategory))
  end

  it "renders the edit event_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", event_category_path(@event_category), "post" do
    end
  end
end
