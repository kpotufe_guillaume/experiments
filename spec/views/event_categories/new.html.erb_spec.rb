require 'spec_helper'

describe "event_categories/new" do
  before(:each) do
    assign(:event_category, stub_model(EventCategory).as_new_record)
  end

  it "renders new event_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", event_categories_path, "post" do
    end
  end
end
