require 'spec_helper'

describe "gift_categories/edit" do
  before(:each) do
    @gift_category = assign(:gift_category, stub_model(GiftCategory))
  end

  it "renders the edit gift_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", gift_category_path(@gift_category), "post" do
    end
  end
end
