require 'spec_helper'

describe "gift_categories/new" do
  before(:each) do
    assign(:gift_category, stub_model(GiftCategory).as_new_record)
  end

  it "renders new gift_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", gift_categories_path, "post" do
    end
  end
end
